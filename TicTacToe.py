board, moves, freeSpaces = [" "] * 9, ["X"] + ["O", "X"] * 4, [str(n) for n in list(range(9))]

def printBoard(b: [str]) -> None:
    print(f"  {b[0]}  |  {b[1]}  |  {b[2]}  \n-----+-----+-----\n  {b[3]}  |", end = "")
    print(f"  {b[4]}  |  {b[5]}  \n-----+-----+-----\n  {b[6]}  |  {b[7]}  |  {b[8]}  ")

def gameWon(b: [str]) -> bool:
    dia = (b[0] == b[4] == b[8] or b[2] == b[4] == b[6]) and b[4] != " "
    row = [b[p] == b[p+1] == b[p+2] and b[p] != " " for p in [0, 3, 6]]
    col = [b[p] == b[p+3] == b[p+6] and b[p] != " " for p in [0, 1, 2]]
    return True if True in [dia] + row + col else False

for move in moves:
    printBoard(board); play = 9
    if gameWon(board): quit("X has won!" if move == "O" else "O has won!")
    while play not in freeSpaces: play = input(f"{move} >>> ")
    if play[0] in freeSpaces: board[int(play)] = move; freeSpaces.remove(play)

printBoard(board), print("Stalemate")